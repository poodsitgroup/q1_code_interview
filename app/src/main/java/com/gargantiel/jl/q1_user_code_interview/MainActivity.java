package com.gargantiel.jl.q1_user_code_interview;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import org.json.JSONException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import structures.Taskcontroller;
import utilities.HttpRequestTool;

public class MainActivity extends AppCompatActivity {

    //Instance Variables
    private Taskcontroller tasklists;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        int PERMISSION_ALL = 1;
        String[] PERMISSIONS = {Manifest.permission.INTERNET, Manifest.permission.ACCESS_NETWORK_STATE};

        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }

        getList();


    }

    // UI Initialize
    public void initialize(){

        ListView items_listview = (ListView) findViewById(R.id.items_listview);
        items_listview.setAdapter(new ArrayAdapter<Taskcontroller>(this, R.layout.list_item_article, R.id.txt_company_name, this.tasklists.getTaskcontrollers()) {

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                LayoutInflater inflater = (LayoutInflater) MainActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View row = inflater.inflate(R.layout.list_item_article, parent, false);

                TextView txt_company_name = (TextView) row.findViewById(R.id.txt_company_name);
                TextView txt_caption = (TextView) row.findViewById(R.id.txt_caption);
                ImageView main_img = (ImageView) row.findViewById(R.id.main_img);
                final ImageView company_img = (ImageView) row.findViewById(R.id.company_img);

                String str = this.getItem(position).getImages().getCaption();


                try {
                    byte spbyte[] = str.getBytes("UTF-8");
                    str = new String( spbyte,"UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                txt_company_name.setText(this.getItem(position).getCompany().getName());
                txt_caption.setText(Html.fromHtml(str)); //

                String urlArticleImage ="http://mootask.com/taskcontroller/getimage?imageId="+
                        this.getItem(position).getImages().getEntityId();
                String urlProfilPic = "http://mootask.com/profilecontroller/showlogobyid?id="+
                        this.getItem(position).getCompany().getEntityId();

                Picasso.with(this.getContext()).load(urlArticleImage).into(main_img);

                Picasso.with(this.getContext()).load(urlProfilPic).into(company_img, new Callback() {
                    @Override
                    public void onSuccess() {
                        Bitmap imageBitmap = ((BitmapDrawable) company_img.getDrawable()).getBitmap();
                        RoundedBitmapDrawable imageDrawable = RoundedBitmapDrawableFactory.create(getResources(), imageBitmap);
                        imageDrawable.setCircular(true);
                        imageDrawable.setCornerRadius(Math.max(imageBitmap.getWidth(), imageBitmap.getHeight()) / 2.0f);
                        company_img.setImageDrawable(imageDrawable);
                    }
                    @Override
                    public void onError() {
                    }
                });

                return row;
            }
        });
    }

    //Getting list from remote sources
    public void getList() {

        AsyncTask getDelivery = new AsyncTask() {

            private HttpRequestTool request;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected Object doInBackground(Object[] params) {

                request = new HttpRequestTool("http://mootask.com/api/taskcontroller/tasks?from=0&max=20", "get");
                request.executeRequest();

                try {
                    MainActivity.this.tasklists = request.getJSONArrayResponse();

                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);

                MainActivity.this.initialize();
            }
        } ;

        getDelivery.execute();
    }

    // Checking different permissions that the application will need
    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }
}
