package utilities;

import android.util.Log;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import interfaces.DataComposite;
import structures.Taskcontroller;

/**
 * Created by jlgargantiel on 13/10/2016.
 */

//This utility is used to create HTTP requests
public class HttpRequestTool {

    public JSONObject jsonResponse = new JSONObject();
    public JSONArray jsonArrayResponse;
    public boolean isDataParsable = true;
    private String url = new String();
    private List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
    private String method;
    private String htmlResponse;
    private int repsonseCode = 000;
    private Taskcontroller taskcontrollerCollection;

    public HttpRequestTool(String url, HashMap<String, String> postvalue, String method){

        this.url = url;
        this.method = method;
        //Generate Post Data
        for (Map.Entry<String, String> entry : postvalue.entrySet()) {
            this.nameValuePair.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
        }
    }

    public HttpRequestTool(String url, String method){

        this.url = url;
        this.method = method;

    }

    public void executeRequest() {
        this.executeGetHttpRequest();
    }

    private void executeGetHttpRequest(){

        HttpClient httpclient = new DefaultHttpClient();

        HttpGet httpGet = new HttpGet(this.url);

        try {
            HttpResponse response = httpclient.execute(httpGet);
            this.htmlResponse = IOUtils.toString(response.getEntity().getContent(), (Charset) response.getEntity().getContentEncoding());
            this.repsonseCode = response.getStatusLine().getStatusCode();

        } catch (ClientProtocolException e) {
            // Log exception
            e.printStackTrace();
        } catch (IOException e) {
            // Log exception
            e.printStackTrace();
        } catch (IllegalStateException e){
            e.printStackTrace();
        }

        Log.d("SE DEBUG", "Response: "+this.htmlResponse);
    }

    public Taskcontroller getJSONArrayResponse(){

        this.taskcontrollerCollection = new Taskcontroller();

        try {
            jsonArrayResponse = new JSONArray(this.htmlResponse);


            for (int index = 0; index < jsonArrayResponse.length(); index++){

                this.jsonResponse = new JSONObject(jsonArrayResponse.get(index).toString());
                Log.d("SE DEBUG", "Response Entity ID: "+this.jsonResponse.get("entityId"));

                Log.d("SE DEBUG", "Response Company: "+this.jsonResponse.get("company"));

                Log.d("SE DEBUG", "Response Image: "+this.jsonResponse.get("images"));

                this.taskcontrollerCollection.addComponents(new JSONObject(jsonArrayResponse.get(index).toString()));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }catch (IllegalStateException e){
            e.printStackTrace();
        }

        return this.taskcontrollerCollection;
    }
}