package interfaces;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by poods on 1/5/16.
 */
public interface DataComposite {

    public ArrayList<HashMap<String, HashMap<String, String>>> getData();
    public void arrangeDataForStorage();
    public void createComponents();
    public void createComponents(String where);
    public void createComponents(ArrayList<HashMap<String, String>> dataSet);
    public void createComponents(JSONObject jsonObject);

}