package structures;

import android.media.Image;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import interfaces.DataComposite;

/**
 * Created by jlgargantiel on 29/03/2017.
 */

public class Taskcontroller implements DataComposite {

    Images images;
    Company company;
    String entityId;
    boolean isParent;

    ArrayList<Taskcontroller> taskcontrollers;

    public Taskcontroller(){
        this.isParent = true;
        this.taskcontrollers = new ArrayList<Taskcontroller>();
    }

    public Taskcontroller(Images images, Company company, String entityId, boolean isParent) {
        this.images = images;
        this.company = company;
        this.entityId = entityId;
        this.isParent = isParent;


    }

    @Override
    public ArrayList<HashMap<String, HashMap<String, String>>> getData() {
        return null;
    }

    @Override
    public void arrangeDataForStorage() {

    }

    @Override
    public void createComponents() {

    }

    @Override
    public void createComponents(String where) {

    }

    @Override
    public void createComponents(ArrayList<HashMap<String, String>> dataSet) {

    }

    @Override
    public void createComponents(JSONObject jsonObject) {
    }

    public void addComponents(JSONObject jsonObject) {

        try { // Create image and company object

            JSONObject company = new JSONObject(jsonObject.get("company").toString());
            JSONArray images = new JSONArray(jsonObject.get("images").toString());



            this.taskcontrollers.add(
                    new Taskcontroller(
                            new Images(new JSONObject(images.get(0).toString())) ,
                    new Company((new JSONObject(jsonObject.get("company").toString()))),
                            jsonObject.get("entityId").toString(),
                            false));

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public ArrayList<Taskcontroller> getTaskcontrollers() {
        return taskcontrollers;
    }

    public Images getImages() {
        return images;
    }

    public Company getCompany() {
        return company;
    }

    public String getEntityId() {
        return entityId;
    }

    @Override
    public String toString() {
        return "Taskcontroller{" +
                "images=" + images +
                ", company=" + company +
                ", entityId='" + entityId + '\'' +
                ", isParent=" + isParent +
                ", taskcontrollers=" + taskcontrollers +
                '}';
    }
}
