package structures;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import interfaces.DataComposite;

/**
 * Created by jlgargantiel on 29/03/2017.
 */

public class Images implements DataComposite {

    String caption;
    String entityId;
    String uploadedDateTime;

    public Images(JSONObject imageObject) {

        try {
            this.caption = imageObject.get("caption").toString();
            this.entityId = imageObject.get("entityId").toString();
            this.uploadedDateTime = imageObject.get("uploadedDateTime").toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public ArrayList<HashMap<String, HashMap<String, String>>> getData() {
        return null;
    }

    @Override
    public void arrangeDataForStorage() {

    }

    @Override
    public void createComponents() {

    }

    @Override
    public void createComponents(String where) {

    }

    @Override
    public void createComponents(ArrayList<HashMap<String, String>> dataSet) {

    }

    @Override
    public void createComponents(JSONObject jsonObject) {

    }

    public String getCaption() {
        return caption;
    }

    public String getEntityId() {
        return entityId;
    }

    public String getUploadedDateTime() {
        return uploadedDateTime;
    }

    @Override
    public String toString() {
        return "Images{" +
                "caption='" + caption + '\'' +
                ", entityId='" + entityId + '\'' +
                ", uploadedDateTime='" + uploadedDateTime + '\'' +
                '}';
    }
}
