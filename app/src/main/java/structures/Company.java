package structures;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import interfaces.DataComposite;

/**
 * Created by jlgargantiel on 29/03/2017.
 */

public class Company implements DataComposite {

    String contactNo;
    String description;
    String entityId;
    String fax;
    String logoEntityId;
    String name;
    String verifyStatus;

    public Company(JSONObject companyJSON) {
        try {
        this.contactNo = companyJSON.get("contactNo").toString();
        this.description = companyJSON.get("description").toString();
            this.entityId = companyJSON.get("entityId").toString();
        this.fax = companyJSON.get("fax").toString();
        this.name = companyJSON.get("name").toString();
        this.verifyStatus = companyJSON.get("verifyStatus").toString();

        this.logoEntityId = (new JSONObject(new JSONArray(companyJSON.get("logos").toString()).get(0).toString())).get("entityId").toString();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public ArrayList<HashMap<String, HashMap<String, String>>> getData() {
        return null;
    }

    @Override
    public void arrangeDataForStorage() {

    }

    @Override
    public void createComponents() {

    }

    @Override
    public void createComponents(String where) {

    }

    @Override
    public void createComponents(ArrayList<HashMap<String, String>> dataSet) {

    }

    @Override
    public void createComponents(JSONObject jsonObject) {

    }

    public String getContactNo() {
        return contactNo;
    }

    public String getDescription() {
        return description;
    }

    public String getEntityId() {
        return entityId;
    }

    public String getFax() {
        return fax;
    }

    public String getLogoEntityId() {
        return logoEntityId;
    }

    public String getName() {
        return name;
    }

    public String getVerifyStatus() {
        return verifyStatus;
    }

    @Override
    public String toString() {
        return "Company{" +
                "contactNo='" + contactNo + '\'' +
                ", description='" + description + '\'' +
                ", entityId='" + entityId + '\'' +
                ", fax='" + fax + '\'' +
                ", logoEntityId='" + logoEntityId + '\'' +
                ", name='" + name + '\'' +
                ", verifyStatus='" + verifyStatus + '\'' +
                '}';
    }
}
